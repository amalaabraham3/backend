from rest_framework import status, viewsets
from rest_framework.views import APIView
from camps.serializers import CampsSerializer
from camps.serializers import CampInhabitantsSerializer
from camps.models import Camp
from camps.models import CampInhabitants
from rest_framework.response import Response

class CampView(APIView):
    def post(self, request, format=None):
        serializer = CampsSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class CampListView(APIView):

    def get(self, request, format=None):
        camps = Camp.objects.all()
        serializer = CampsSerializer(camps, many=True)
        return Response(serializer.data)

class CampInhabitantsView(APIView):

    def get(self, request, format=None):
        camps = Camp.objects.all()
        serializer = CampInhabitantsSerializer(camps, many=True)
        return Response(serializer.data)

class CampInhabitantsListView(APIView):

    def post(self, request, format=None):
        serializer = CampsSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
