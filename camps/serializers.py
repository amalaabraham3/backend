from rest_framework import serializers
from camps.models import Camp
from camps.models import CampInhabitants

class CampsSerializer(serializers.ModelSerializer):

    class Meta:

        model = Camp
        fields = '__all__'


class CampInhabitantsSerializer(serializers.ModelSerializer):

    camp = serializers.SerializerMethodField()

    class Meta:

        model = CampInhabitants
        fields = '__all__'

    def validate_camp(self, value):
        return Camp.objects.get(id=self.request.id)

    def get_camp(self, camp):
        return camp.id
